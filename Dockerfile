FROM node:current-alpine as build

WORKDIR /app

ADD package* ./
RUN npm ci

ADD ./src ./src
RUN npm run build

FROM nginx:alpine

COPY --from=build /app/dist/* /usr/share/nginx/html/
